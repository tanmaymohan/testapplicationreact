import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <h1>Hi, Welcome to ECS Fargate test</h1>
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://tanmaymohan.com"
          target="_blank"
          rel="noopener noreferrer"
        >
          Visit my Site
        </a>
      </header>
    </div>
  );
}

export default App;
